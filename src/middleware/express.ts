import {Response, Router} from 'express';
import {SwaggerService} from '../service';
import {build, ISwaggerBuildDefinition} from '../builder';
import {resolve} from "path";

export interface ISwaggerExpressOptions {
  /**
   * Path of resource.
   * Default is "/api-docs/swagger.json".
   */
  path?: string;

  /**
   * Swagger Definition.
   */
  definition?: ISwaggerBuildDefinition;
}

export function swagger(options: ISwaggerExpressOptions = {}): Router {
  const {path = '/api-docs/swagger.json', definition} = options;
  if (definition) {
    build(definition);
  }

  const {static: content} = require('express');

  return Router()
    .get(path, (_: any, response: Response) => response.json(SwaggerService.getData()))
    .use(path.replace(/\.json$/, '')
      , content(resolve(__dirname + '../../../../public')))
    .use(path.replace(/\.json$/, '/assets')
      , content(require.resolve('swagger-ui-dist').replace('index.js', '')));
}
