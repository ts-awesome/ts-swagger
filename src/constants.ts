export const JSON = 'application/json';
export const XML = 'application/xml';
export const ZIP = 'application/zip';
export const PDF = 'application/pdf';

export const X_WWW_FORM_URLENCODED = 'application/x-www-form-urlencoded';
export const FORM_DATA = 'multipart/form-data';

export const TEXT_PLAIN = 'text/plain';
export const TEXT_HTML = 'text/html';

export const PNG = 'image/png';
export const GIF = 'image/gif';
export const JPEG = 'image/jpeg';

export const STRING = 'string';
export const NUMBER = 'number';
export const INTEGER = 'integer';
export const BOOLEAN = 'boolean';
export const ARRAY = 'array';
export const OBJECT = 'object';

export const QUERY = 'query';

export const INT32 = 'int32';
export const INT64 = 'int64';
export const FLOAT = 'float';
export const DOUBLE = 'double';
export const BYTE = 'byte';
export const BINARY = 'binary';
export const DATE = 'date';
export const DATE_TIME = 'date-time';
export const PASSWORD = 'password';

export const Produce = {
  FORM_DATA,
  GIF,
  JPEG,
  JSON,
  PDF,
  PNG,
  TEXT_HTML,
  TEXT_PLAIN,
  XML,
  X_WWW_FORM_URLENCODED,
  ZIP,
};

export const Scheme = {
  HTTP: 'http',
  HTTPS: 'https',
};

export const Consume = {
  JSON,
  XML,
};

export type ModelType = typeof ARRAY | typeof OBJECT;
export const Model = {
  Type: {
    OBJECT,
    ARRAY,
  },
};

export type PropertyFormat = typeof INT32
  | typeof INT64
  | typeof FLOAT
  | typeof DOUBLE
  | typeof BYTE
  | typeof BINARY
  | typeof DATE
  | typeof DATE_TIME
  | typeof PASSWORD;
export type PropertyType =
    typeof ARRAY
  | typeof BOOLEAN
  | typeof INTEGER
  | typeof NUMBER
  | typeof OBJECT
  | typeof STRING;
export type PropertyItemType =
    typeof BOOLEAN
  | typeof INTEGER
  | typeof NUMBER
  | typeof STRING;
export const Property = {
  Format: {
    INT32,
    INT64,
    FLOAT,
    DOUBLE,
    BYTE,
    BINARY,
    DATE,
    DATE_TIME,
    PASSWORD,
  },
  ItemType: {
    BOOLEAN,
    INTEGER,
    NUMBER,
    STRING,
  },
  Type: {
    ARRAY,
    BOOLEAN,
    INTEGER,
    NUMBER,
    OBJECT,
    STRING,
  },
};

export type ParameterIn = 'body' | 'formData' | 'path' | typeof QUERY;
export type ParameterType =
    typeof ARRAY
  | typeof BOOLEAN
  | typeof INTEGER
  | typeof NUMBER
  | typeof OBJECT
  | typeof STRING;
export const Parameter = {
  In: {
    BODY: 'body',
    FORM_DATA: 'formData',
    PATH: 'path',
    QUERY,
  },
  Type: {
    ARRAY,
    BOOLEAN,
    INTEGER,
    NUMBER,
    OBJECT,
    STRING,
  },
};

export type ResponseType =
    typeof ARRAY
  | typeof BOOLEAN
  | typeof INTEGER
  | typeof NUMBER
  | typeof OBJECT
  | typeof STRING;
export const Response = {
  Type: {
    ARRAY,
    BOOLEAN,
    INTEGER,
    NUMBER,
    OBJECT,
    STRING,
  },
};

export const Security = {
  In: {
    HEADER: 'header',
    QUERY,
  },
  Type: {
    API_KEY: 'apiKey',
    BASIC_AUTHENTICATION: 'basic',
  },
};
