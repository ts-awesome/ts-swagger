import {
  IApiModelArgs,
  IApiModelPropertyArgs,
  IApiOperationGetArgs,
  IApiOperationPostArgs,
  IApiPathArgs
} from './decorators';
import {
  IApiBodyOperationArgsBaseParameter,
  IApiOperationArgsBase,
  IApiOperationArgsBaseParameter,
  IApiOperationArgsBaseResponse,
} from './decorators/interfaces';
import {
  ISwagger,
  ISwaggerExternalDocs,
  ISwaggerInfo,
  ISwaggerOperation,
  ISwaggerOperationParameter,
  ISwaggerOperationResponse,
  ISwaggerSecurityDefinition,
} from './interfaces';
import {Consume, Model, Parameter, Produce, Scheme, Response} from './constants';
import {ISwaggerBuildDefinitionModel} from './builder';
import sha1 = require('sha1');

interface IPath {
  path: string;
  get?: ISwaggerOperation;
  post?: ISwaggerOperation;
  put?: ISwaggerOperation;
  patch?: ISwaggerOperation;
  delete?: ISwaggerOperation;
}

interface IController {
  path: string;
  paths: Record<string, IPath>;
  name: string;
  description?: string;
  security?: Record<string, any[]>;
  deprecated?: boolean;
}

const assert = {
  ok(cond, msg) {
    if (!cond) {
      throw new Error(msg);
    }
  }
};

class SwaggerServiceImpl {
  private data: ISwagger;
  private controllerMap: Record<string, IController> = {};

  private modelsMap: Record<string, ISwaggerBuildDefinitionModel> = {};
  private globalResponses: Record<string, IApiOperationArgsBaseResponse> = {};

  public constructor() {
    this.initData();
  }

  // noinspection JSUnusedGlobalSymbols
  public resetData(): void {
    this.initData();
  }

  public getData(): ISwagger {
    return cloneDeep(this.data);
  }

  public setBasePath(basePath: string): void {
    this.data.basePath = basePath;
  }

  public setOpenapi(openapi: string): void {
    this.data.openapi = openapi;
  }

  public setInfo(info: ISwaggerInfo): void {
    this.data.info = info;
  }

  public setSchemes(schemes: string[]): void {
    this.data.schemes = schemes;
  }

  public setProduces(produces: string[]): void {
    this.data.produces = produces;
  }

  public setConsumes(consumes: string[]): void {
    this.data.consumes = consumes;
  }

  public setHost(host: string): void {
    this.data.host = host;
  }

  public setDefinitions(models: Record<string, ISwaggerBuildDefinitionModel>): void {
    this.data.definitions = {
      ...this.data.definitions,
      ...objectMap(models, model => {
        return {
          type: Model.Type.OBJECT,
          required: objectEntities(model.properties)
            .filter(([, {required: _}]) => _).map(([k]) => k),
          description: model.description || undefined,
          properties: objectMap(model.properties, ({model, type, ...property}) => ({
            ...property,
            ...buildSchema(model, type),
          }))
        };
      }),
    };
  }

  public setExternalDocs(externalDocs: ISwaggerExternalDocs): void {
    this.data.externalDocs = externalDocs;
  }

  public setGlobalResponses(globalResponses: Record<string, IApiOperationArgsBaseResponse>): void {
    this.globalResponses = buildOperationResponses(globalResponses);
  }

  public addPath(args: IApiPathArgs, target: any): void {
    this.controllerMap[target.name] = {
      paths: {},
      ...this.controllerMap[target.name],
      ...args,
    };
  }

  public addOperationGet(args: IApiOperationGetArgs, target: any): void {
    assert.ok(args, 'Args are required.');
    assert.ok(args.responses, 'Responses are required.');
    if (args.parameters) {
      assert.ok(!args.parameters.body, 'Parameter body is not required.');
    }
    this.addOperation('get', args, target);
  }

  public addOperationPost(args: IApiOperationPostArgs, target: any): void {
    assert.ok(args, 'Args are required.');
    assert.ok(args.parameters, 'Parameters are required.');
    assert.ok(args.responses, 'Responses are required.');
    this.addOperation('post', args, target);
  }

  public addOperationPut(args: IApiOperationPostArgs, target: any): void {
    assert.ok(args, 'Args are required.');
    assert.ok(args.parameters, 'Parameters are required.');
    assert.ok(args.responses, 'Responses are required.');
    this.addOperation('put', args, target);
  }

  public addOperationPatch(args: IApiOperationPostArgs, target: any): void {
    assert.ok(args, 'Args are required.');
    assert.ok(args.parameters, 'Parameters are required.');
    assert.ok(args.responses, 'Responses are required.');
    this.addOperation('patch', args, target);
  }

  public addOperationDelete(args: IApiOperationPostArgs, target: any): void {
    assert.ok(args, 'Args are required.');
    assert.ok(args.parameters, 'Parameters are required.');
    assert.ok(!args.parameters!.body, 'Parameter body is not required.');
    assert.ok(args.responses, 'Responses are required.');
    this.addOperation('delete', args, target);
  }

  public addSecurityDefinitions(securityDefinitions: Record<string, ISwaggerSecurityDefinition>): void {
    this.data.securityDefinitions = securityDefinitions;
  }

  public buildSwagger(): void {
    const data: ISwagger = cloneDeep(this.data);
    objectEntities(this.controllerMap)
      .forEach(([, controller]) => {
        objectEntities(controller.paths!)
          .forEach(([, path]) => {
            const key = `${controller.path}${path.path || ''}`;
            Object.keys(path)
              .filter(method => ['get', 'post', 'put', 'patch', 'delete'].indexOf(method) >= 0)
              .forEach(method => {
                data.paths![key] = {
                  ...data.paths![key],
                  [method]: this.buildSwaggerOperation(path[method], controller),
                };
              });
        });

        data.tags!.push({
          name: upperFirst(controller.name!),
          description: controller.description || '',
        });
      });

    this.data = data;
  }

  public addApiModelProperty(
    args: IApiModelPropertyArgs,
    target: any,
    propertyKey: string | symbol,
    propertyType: string
  ) {
    const {type = propertyType.toLowerCase(), model = null} = args || {};

    this.getModelDef(target.constructor.name)
      .properties[propertyKey.toString()] = {
        ...args,
        ...buildSchema(model, type),
      };

    this.setDefinitions(this.modelsMap);
  }

  public addApiModel(args: IApiModelArgs, target: any): any {
    const definitionKey = target.name;

    const model = this.getModelDef(definitionKey);
    model.description = (args && args.description) || model.description;

    if (args && args.name) {
      const name: string = upperFirst(args.name);
      this.modelsMap[name] = this.modelsMap[definitionKey];
      if (name !== definitionKey) {
        delete this.modelsMap[definitionKey];
        delete this.data.definitions[definitionKey];
      }
    }

    this.setDefinitions(this.modelsMap);
  }

  private getModelDef(definitionKey: string) {
    if (!this.modelsMap[definitionKey]) {
      this.modelsMap[definitionKey] = {
        properties: {},
      };
    }

    return this.modelsMap[definitionKey];
  }

  private initData(): void {
    this.controllerMap = {};
    this.data = {
      basePath: '/',
      info: {
        title: '',
        version: '',
      },
      paths: {},
      tags: [],
      schemes: [Scheme.HTTP],
      produces: [Produce.JSON],
      consumes: [Consume.JSON],
      definitions: {},
      swagger: '2.0',
    };
  }

  private addOperation(operation: 'get'|'post'|'put'|'patch'|'delete', args: IApiOperationArgsBase, target: any): void {
    const constructorName = target.constructor.name;

    const currentController = this.controllerMap[constructorName] = {
      name: constructorName,
      path: '',
      paths: {},
      ...this.controllerMap[constructorName],
    };

    const path = args.path || '/';
    const currentPath = currentController.paths[path] = {
      path,
      ...currentController.paths[path],
    };

    switch (operation.toLowerCase()) {
      case 'get':
      case 'post':
      case 'put':
      case 'patch':
      case 'delete':
        currentPath[operation.toLowerCase()] = buildOperation(args, sha1(`operation::${constructorName}::${path}`));
        break;
      default:
        console.warn(`Skipping unknown operation ${JSON.stringify(operation)}`);
    }
  }

  private buildSwaggerOperation(
    operation: ISwaggerOperation,
    controller: IController
  ): ISwaggerOperation {
    return {
      produces: this.data.produces,
      consumes: this.data.consumes,
      deprecated: controller.deprecated,
      ...operation,
      security: controller.security ? buildOperationSecurity(controller.security) : operation.security,
      responses: {
        ...this.globalResponses,
        ...operation.responses,
      },
      tags: [upperFirst(controller.name)],
    };
  }
}

function buildOperation(args: IApiOperationArgsBase, operationId: string): ISwaggerOperation {
  return {
    operationId,
    tags: [],
    description: args.description || undefined,
    summary: args.summary || undefined,
    produces: args.produces && args.produces.length ? args.produces : undefined,
    consumes: args.consumes && args.consumes.length ? args.consumes : undefined,
    deprecated: args.deprecated || undefined,
    parameters: !args.parameters ? undefined : [
      ...buildParameters(Parameter.In.PATH, args.parameters.path),
      ...buildParameters(Parameter.In.QUERY, args.parameters.query),
      ...buildBodyOperationParameter(args.parameters.body),
      ...buildParameters(Parameter.In.FORM_DATA, args.parameters.formData),
    ],
    responses: args.responses ? buildOperationResponses(args.responses) : undefined,
    security: args.security ? buildOperationSecurity(args.security) : undefined,
  };
}

const ResponseDict: Record<string, string> = {
  '200': 'Success',
  '201': 'Created',
  '202': 'Accepted',
  '203': 'Non-Authoritative Information',
  '204': 'No Content',
  '205': 'Reset Content',
  '206': 'Partial Content',

  '400': 'Client error and Bad Request',
  '401': 'Client error and Unauthorized',
  '403': 'Client error and Forbidden',
  '404': 'Client error and Not Found',
  '406': 'Client error and Not Acceptable',

  '500': 'Internal Server Error',
  '501': 'Not Implemented',
  '504': 'Service Unavailable',
};

function buildOperationResponses(responses: Record<string, IApiOperationArgsBaseResponse>): Record<string, ISwaggerOperationResponse> {
  return objectMap(responses, ({description, model, type}, responseIndex) => ({
    description: description || ResponseDict[responseIndex] || undefined,
    schema: buildSchema(model, type),
  }));
}

function buildSchema(model, type): any {
  if (!model && type) {
    return {type: type};
  }

  if (!model) {
    return undefined;
  }

  const ref = {$ref: buildRef(model)};

  return type === Response.Type.ARRAY
      ? {
        items: ref,
        type: Response.Type.ARRAY,
      } : {
        ...ref,
        type: Response.Type.OBJECT,
      };
}

function buildBodyOperationParameter(param?: IApiBodyOperationArgsBaseParameter): ISwaggerOperationParameter[] {
  if (!param) {
    return [];
  }

  const {name, model, properties} = param;

  return [{
    ...param,
    in: 'body',
    name: name ? name : 'body',
    schema: (model && {
          $ref: buildRef(model)
        })
      || (properties && {
          type: 'object',
          required: Object.keys(properties).filter(k => properties[k].required),
          properties: objectMap(properties, ({type}) => ({type}))
        })
      || {},
  }];
}

function buildOperationSecurity(argsSecurity: Record<string, any[]>): Array<Record<string, any[]>> {
  return objectEntities(argsSecurity)
    .map(([securityIndex, security]) => ({
      [securityIndex]: security
    }));
}

function buildParameters(type: string, parameters?: Record<string, IApiOperationArgsBaseParameter>): ISwaggerOperationParameter[] {
  return objectEntities(parameters || {})
    .map(([parameterIndex, parameter]) => ({
      ...parameter,
      name: parameter.name ? parameter.name : parameterIndex,
      in: type,
      type: parameter.type,
    }));
}

function objectMap<K, T>(obj: Record<string, T>, cb: (v: T, k: string) => K): Record<string, K> {
  // noinspection CommaExpressionJS
  return Object
    .keys(obj)
    .map((k: string): [string, K] => [k, cb(obj[k], k)])
    .reduce((p, [k, v]) => (p[k] = v, p), {} as Record<string, K>);
}

function objectEntities<T>(obj: Record<string, T>): Array<[string, T]> {
  return Object.keys(obj).map((k: string): [string, T] => [k, obj[k]]);
}

function upperFirst(def: string): string {
  if (!def) return def;
  const name = def.split('');
  name[0] = name[0].toUpperCase();
  return name.join('');
}

function buildRef(definition: string | Function | symbol): string {
  const name = typeof definition === 'function' ? definition.name : definition.toString();
  return `#/definitions/${upperFirst(name)}`;
}

function cloneDeep<T>(x: T): T {
  return JSON.parse(JSON.stringify(x)) as T;
}

export const SwaggerService = new SwaggerServiceImpl();
