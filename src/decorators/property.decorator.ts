import 'reflect-metadata';

import { SwaggerService } from '../service';
import {IApiModelPropertyArgs} from "./interfaces";

export {IApiModelPropertyArgs}

export function ApiModelProperty(args?: IApiModelPropertyArgs): PropertyDecorator {
  return (target: any, propertyKey: string | symbol) => {
    const {name} = Reflect.getMetadata('design:type', target, propertyKey);
    SwaggerService.addApiModelProperty(args || {}, target, propertyKey, name);
  };
}
