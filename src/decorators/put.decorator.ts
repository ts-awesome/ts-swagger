import { SwaggerService } from '../service';
import { IApiOperationArgsBase } from './interfaces';
export interface IApiOperationPutArgs extends IApiOperationArgsBase {}

export function ApiOperationPut(args: IApiOperationPutArgs): MethodDecorator {
  return (target: any) => SwaggerService.addOperationPut(args, target);
}
