import { SwaggerService } from '../service';
import { IApiOperationArgsBase } from './interfaces';
export interface IApiOperationPostArgs extends IApiOperationArgsBase {}

export function ApiOperationPost(args: IApiOperationPostArgs): MethodDecorator {
  return (target: any) => SwaggerService.addOperationPost(args, target);
}
