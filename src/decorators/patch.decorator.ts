import { SwaggerService } from '../service';
import { IApiOperationArgsBase } from './interfaces';
export interface IApiOperationPatchArgs extends IApiOperationArgsBase {}

export function ApiOperationPatch(args: IApiOperationPatchArgs): MethodDecorator {
  return (target: any) => SwaggerService.addOperationPatch(args, target);
}
