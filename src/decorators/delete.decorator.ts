import { SwaggerService } from '../service';
import { IApiOperationArgsBase } from './interfaces';
export interface IApiOperationDeleteArgs extends IApiOperationArgsBase {}

export function ApiOperationDelete(args: IApiOperationDeleteArgs): MethodDecorator {
  return (target: any) => SwaggerService.addOperationDelete(args, target);
}
