import { SwaggerService } from '../service';

export interface IApiModelArgs {
  description?: string;
  name?: string;
}

export function ApiModel(args?: IApiModelArgs): ClassDecorator {
  return (target: any) => SwaggerService.addApiModel(args || {}, target);
}
