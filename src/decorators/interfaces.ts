import {
  ParameterType,
  PropertyFormat,
  PropertyItemType,
  PropertyType,
  ResponseType,
} from "../constants";

export interface IApiOperationArgsBaseParameter {
  name?: string; // Override [key: string]. Default [key: string].
  description?: string;
  type?: string;
  required?: boolean;
  format?: string;
  minimum?: number;
  maximum?: number;
  default?: number;
  deprecated?: boolean;
  allowEmptyValue?: boolean;
}

export interface IApiPropertyBodyOperationArgsBaseParameter {
  type: ParameterType;
  required?: boolean;
}

export interface IApiBodyOperationArgsBaseParameter extends IApiOperationArgsBaseParameter {
  properties?: Record<string, IApiPropertyBodyOperationArgsBaseParameter>;
  model?: Function | string;
}

export interface IApiOperationArgsBaseResponse {
  description?: string;
  type?: ResponseType;
  model?: Function | string;
}

export interface IApiOperationArgsBaseParameters {
  path?: Record<string, IApiOperationArgsBaseParameter>;
  query?: Record<string, IApiOperationArgsBaseParameter>;
  body?: IApiBodyOperationArgsBaseParameter; // use only for POST, PUT and PATCH
  formData?: Record<string, IApiOperationArgsBaseParameter>;
}

export interface IApiOperationArgsBase {
  description?: string;
  summary?: string;
  produces?: string[];
  consumes?: string[];
  path?: string;
  parameters?: IApiOperationArgsBaseParameters;
  responses: Record<string, IApiOperationArgsBaseResponse>;
  security?: Record<string, any[]>;
  deprecated?: boolean;
}

export interface IApiModelPropertyArgs {
  required?: boolean;
  format?: PropertyFormat;
  type?: PropertyType;
  description?: string;
  enum?: string[];
  model?: Function | string;
  itemType?: PropertyItemType;
  example?: any[];
}
