import { SwaggerService } from '../service';
import { IApiOperationArgsBase } from './interfaces';
export interface IApiOperationGetArgs extends IApiOperationArgsBase {}

export function ApiOperationGet(args: IApiOperationGetArgs): MethodDecorator {
  return (target: any) => SwaggerService.addOperationGet(args, target);
}
